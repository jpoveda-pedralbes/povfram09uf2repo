package cat.inspedralbes.povfra;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AdsExecutorScheduled {
	public static void main(final String... args) throws InterruptedException, ExecutionException {
		String[] adsMati 	= { "adM1", "adM2", "adM3", "adM4" };
		String[] adsMigdia 	= { "adMg1", "adMg2", "adMg3", "adMg4", "adMg5" };
		String[] adsTarda 	= { "adT1", "adT2", "adT3", "adT4", "adT5", "adT6" };
		 
		Runnable obMati 	= new Advertisments(adsMati);
		Runnable obMigdia 	= new Advertisments(adsMigdia);
		Runnable obTarda  	= new Advertisments(adsTarda);
		
		ScheduledExecutorService schExService = Executors.newSingleThreadScheduledExecutor();
		
		ScheduledFuture<?> tascaMati = schExService.scheduleWithFixedDelay(obMati, 0, 5, TimeUnit.SECONDS);
		schExService.awaitTermination(30, TimeUnit.SECONDS);
		tascaMati.cancel(true);
		
		ScheduledFuture<?> tascaMigdia = schExService.scheduleWithFixedDelay(obMigdia, 10, 1, TimeUnit.SECONDS);
		schExService.awaitTermination(20, TimeUnit.SECONDS);
		tascaMigdia.cancel(true);
		
		ScheduledFuture<?> tascaTarda = schExService.scheduleWithFixedDelay(obTarda, 10, 2, TimeUnit.SECONDS);
		schExService.awaitTermination(22, TimeUnit.SECONDS);
		tascaTarda.cancel(true);
		
		schExService.shutdownNow();
	}
}