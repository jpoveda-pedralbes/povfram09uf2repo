package cat.inspedralbes.povfra;

import java.io.IOException;

public class PovFraE1Ping {

	public static void main(String[] args) {

		ProcessBuilder pb = new ProcessBuilder("ping", "-c", "4", "google.com");
		pb.inheritIO();
		
		Process p;
		try {
			p = pb.start();
			
			p.waitFor();
			//Si tot ha anat be, el valor de exitValue sera 0 i sino sera qualsevol altre numero
			int exitValue = p.exitValue();
			
			System.out.println(exitValue);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}
