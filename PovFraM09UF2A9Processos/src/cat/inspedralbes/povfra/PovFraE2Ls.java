package cat.inspedralbes.povfra;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PovFraE2Ls {
	public static void main(String[] args) {

		ProcessBuilder pb = new ProcessBuilder("ls", "-la", "/home/ausias");
		
		Process p;
		try {
			p = pb.start();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String ln;
			
			int i = 0;
			while( (ln = br.readLine()) != null /*&& i <= 10*/ ) {
				System.out.println(i + ". " + ln);
				i++;
			}
			
			int returnCode = p.waitFor();
			System.out.println(returnCode);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
