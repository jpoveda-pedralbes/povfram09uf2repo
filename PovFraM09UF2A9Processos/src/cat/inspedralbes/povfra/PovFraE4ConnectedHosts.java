package cat.inspedralbes.povfra;

import java.io.IOException;
import java.util.ArrayList;

public class PovFraE4ConnectedHosts {
	public static void main(String[] args) {
		/*
		 * SOLUCIO CONCURRENT
		 * */
		
		int connectats = 0;
		
		ProcessBuilder pb;
		Process p;
		
		ArrayList<Process> processArray = new ArrayList<>();
		
		for(int i = 101; i<=127; i++) {
			
			try {
				pb = new ProcessBuilder("ping", "-c", "1", "192.168.205."+i);
				p = pb.start();
				processArray.add(p);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i<processArray.size(); i++) {
			
			try {
				if( processArray.get(i).waitFor() == 0 && !processArray.get(i).isAlive()) {
					connectats++;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		System.out.println("Hi ha " + connectats + " ordinadors on-line.");
		
		/*
		 * SOLUCIO SECUENCIAL
		 * */
		
//		ProcessBuilder pb;
//
//		for(int i = 101; i<=127; i++) {
//			//ping -c 1 192.168.205.108
//			pb = new ProcessBuilder("ping", "-c", "1", "192.168.205."+i);
//			try {
//				Process p;
//				p = pb.start();
//				int returnValue = p.waitFor();
//				
//				if( returnValue == 0 ) {
//					System.out.println("192.168.205."+i+" - "+returnValue);
//				}
//				
//			} catch (IOException | InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
	}
}
