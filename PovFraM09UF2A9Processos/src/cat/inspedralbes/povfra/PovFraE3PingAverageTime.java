package cat.inspedralbes.povfra;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PovFraE3PingAverageTime {
	
	public static void main(String[] args) {
		
		String url = args[0];
		String paquetsMitjana = args[1]; 
		
		float msTime = 0;
		
		ProcessBuilder pb = new ProcessBuilder("ping", "-c", paquetsMitjana, url);
	
		Process p;
		try {
			p = pb.start();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ln;

			while( (ln = br.readLine()) != null ) {
				System.out.println(ln);
				if( ln.contains("time=") ) {
					msTime += Float.parseFloat(ln.substring(ln.length()-7, ln.length()-3)); 
				}
			}
			
			msTime = (msTime/Float.parseFloat(paquetsMitjana));
			p.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	
		System.out.println("El temps mig de realitzar un ping a " + url + " és de " + msTime + " ms. Mitjana realitzada amb "+ paquetsMitjana +" paquets.");
	}
}
