package cat.inspedralbes.povfra;

class PovFraA5Client extends Thread{	
	private PovFraA5Hotel hr;
	private int rooms;	
	public PovFraA5Client(PovFraA5Hotel hotelRooms,int numRoomsToBook)
	{
		hr=hotelRooms;
		rooms=numRoomsToBook;
	}	
	public void run(){		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		hr.bookingPetition(rooms);
	}
}
