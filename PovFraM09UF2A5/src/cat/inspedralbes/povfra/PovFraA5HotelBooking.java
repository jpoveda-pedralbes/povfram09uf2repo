package cat.inspedralbes.povfra;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PovFraA5HotelBooking {
	
	private final static Logger LOGGER = Logger.getLogger(PovFraA5HotelBooking.class.getName());
	
	public static void main(String[] args) throws InterruptedException {		
		
		LOGGER.setLevel(Level.INFO);
				
		PovFraA5Hotel hr= new PovFraA5Hotel("IAM",new CogNomA5Address("Avinguda d'Esplugues, 38","Barcelona","08034"),40);		
		hr.setFreeRooms(5);			
		
		PovFraA5Client client1 = new PovFraA5Client(hr,4);		
		PovFraA5Client client2 = new PovFraA5Client(hr,3);	
		PovFraA5Client client3 = new PovFraA5Client(hr,2);			
		
		client1.setName("Client A");				
		client2.setName("Client B");			
		client3.setName("Client C");			
		
		client1.start();		
		client2.start();
		client3.start();		
	}
}
