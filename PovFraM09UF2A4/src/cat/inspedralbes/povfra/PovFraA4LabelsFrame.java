package cat.inspedralbes.povfra;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class PovFraA4LabelsFrame extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public JPanel mainPanel;
	public ArrayList<PovFraA4MovingLabel> quadresLbl = new ArrayList<>();
	public Random rand = new Random();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PovFraA4LabelsFrame frame = new PovFraA4LabelsFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	public PovFraA4LabelsFrame() {
		setTitle("PovFraA4MovingLabels");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBounds(12, 12, 424, 214);
		contentPane.add(mainPanel);

		JButton stopButton = new JButton("Stop");
		stopButton.addActionListener(this);
		stopButton.setBounds(319, 234, 117, 25);
		contentPane.add(stopButton);
		
		JButton addButton = new JButton("Add");
		addButton.addActionListener(this);
		addButton.setBounds(207, 234, 117, 25);
		contentPane.add(addButton);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String button = e.getActionCommand();
		
		switch(button) {
		case "Add":
			quadresLbl.add(new PovFraA4MovingLabel(mainPanel));
			Thread fil = new Thread(quadresLbl.get(quadresLbl.size()-1));
			fil.start();
			System.out.println(Thread.activeCount());
			break;
		case "Stop":
			System.out.println(Thread.activeCount());
			if( !quadresLbl.isEmpty() ) {
				int i =rand.nextInt(quadresLbl.size());
				quadresLbl.get(i).stop();
				quadresLbl.remove(i);
			}
			break;
		}
		
		
	}
}
