package cat.inspedralbes.povfra;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PovFraA4MovingLabel extends  JLabel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	public boolean stop = false;
	
	public PovFraA4MovingLabel (JPanel panel){

		this.panel = panel;
		setOpaque(true);
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(10, 10));
		setAlignmentX(LEFT_ALIGNMENT);
		panel.add(this);
		panel.repaint();
		panel.revalidate();
	}
	
	@Override
	public void run() {

		try {
			
			int posicioX   = this.getLocation().x;
			int posicioY   = this.getLocation().y;
			int ampleLabel = this.getPreferredSize().width;
			int alturaLabel = this.getPreferredSize().height;
			
			int i = posicioX;
			int j = posicioY;
			
			while(!stop) {
				Thread.sleep(500);
				j = j+alturaLabel;
				i = i+ampleLabel;
				this.setLocation(i, j);
				panel.repaint();
			}
				
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void stop() {
		stop = true;
		setOpaque(false);
	}

}
