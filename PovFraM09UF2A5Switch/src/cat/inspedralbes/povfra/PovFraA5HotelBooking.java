package cat.inspedralbes.povfra;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;


// https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/package-summary.html
// https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/Logger.html
// https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/Handler.html
// https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/Level.html
public class PovFraA5HotelBooking {
		
	private static Scanner inputReader;

	public static void main(String[] args) throws InterruptedException {		
		
		int opcioEscollida = 0;
		int habsLliures = 5;
		int numClients = 3;
		int numHabitacions = 0;
		
		String arrayNoms[] = {"Client A", "Client B", "Client C"};
		
		ArrayList<PovFraA5Client> clientsArray = new ArrayList<>();
		
		inputReader = new Scanner(System.in);

		
		///////////////////////////////////////////////////////////////////////
		// Create a file handler object for logging purposes 
        FileHandler handler=null;
		try {
			handler = new FileHandler("logBooking.txt");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        handler.setFormatter(new SimpleFormatter()); 
        
        ///////////////////////////////////////////////////////////////////////
        // Add file handler   
 		PovFraA5Hotel.LOGGER.addHandler(handler);
 			
 		// Set Logger level to FINEST 
 		PovFraA5Hotel.LOGGER.setLevel(Level.ALL);

        PovFraA5Hotel hr= new PovFraA5Hotel("IAM",new PovFraA5Address("Avinguda d'Esplugues, 38","Barcelona","08034"),habsLliures);        
		do {
			
			System.out.println("Que vols fer?");
			System.out.println("1. Reservar habitació");
			System.out.println("2. Veure si hi ha disponibilitat");
			System.out.println("3. Veure ràtio d'ocupació");
			System.out.println("4. Veure adreça");
			System.out.println("5. Veure habitacions lliures");
			System.out.println("6. Sortir");
			
			opcioEscollida = Integer.parseInt(inputReader.nextLine());
			
			switch (opcioEscollida) {
			case 1:
				System.out.println("Tenim " + hr.getFreeRooms() + " habitacions lliures. Quantes habitacions vols?");
				numHabitacions = Integer.parseInt(inputReader.nextLine());

				clientsArray.add(new PovFraA5Client(hr,PovFraSimulationType.BOOKING,numHabitacions));
				break;
			case 2:
				System.out.println("Quantes habitacions vols reservar? ");
				numHabitacions = Integer.parseInt(inputReader.nextLine());
				
				clientsArray.add(new PovFraA5Client(hr,PovFraSimulationType.AVAILABILITY,numHabitacions));
				break;	
			case 3:
				clientsArray.add(new PovFraA5Client(hr,PovFraSimulationType.OCCUPANCY_RATE,0));
				break;	
			case 4:
				clientsArray.add(new PovFraA5Client(hr,PovFraSimulationType.GET_ADDRESS,0));
				break;	
			case 5:
				clientsArray.add(new PovFraA5Client(hr,PovFraSimulationType.GET_FREE_ROOMS,0));
				break;	
			}
			
			System.out.println("Mida clientsArray: " + clientsArray.size());
			
		}while(opcioEscollida != 6 || clientsArray.size()<numClients);
		
		for(int i = 0; i<=clientsArray.size();i++) {
			PovFraA5Client fil = clientsArray.get(i);
			fil.setName(arrayNoms[i]);		
			System.out.println(arrayNoms[i]);
			fil.start();
		}
		
		
	}
}
