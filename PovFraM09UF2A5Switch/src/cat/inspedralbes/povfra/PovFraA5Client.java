package cat.inspedralbes.povfra;

class PovFraA5Client extends Thread{	
	private PovFraA5Hotel hr;
	private int rooms;	
	private PovFraSimulationType type;

	public PovFraA5Client(PovFraA5Hotel hotelRooms, PovFraSimulationType type)
	{
		hr=hotelRooms;
		this.type=type;
	}
	
	public PovFraA5Client(PovFraA5Hotel hotelRooms,PovFraSimulationType type,int rooms)
	{		
		hr=hotelRooms;
		this.type=type;
		this.rooms=rooms;
	}	

	public void run(){		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		setSimulation();		
	}
		
	private void setSimulation(){		
		switch (type) {
		case BOOKING:
			hr.bookingPetition(rooms);
			break;
		case PROCESS_PETITION:
			hr.processPetition(rooms)	;
			break;
		case AVAILABILITY:
			hr.getAvailability(rooms);
			break;	
		case SET_FREE_ROOMS:
			hr.setFreeRooms(rooms);
			break;				
		case OCCUPANCY_RATE:
			hr.getOccupancyRate();
			break;
		case GET_ADDRESS:
			hr.getAddress() ;
			break;
		case GET_ROOMS:
			hr.getRooms();
			break;	
		case GET_FREE_ROOMS:
			hr.getFreeRooms();
			break;		
		}
	}
}
