package cat.inspedralbes.povfra;

enum PovFraSimulationType {
    BOOKING,
    PROCESS_PETITION,
    SET_FREE_ROOMS,
    AVAILABILITY,
    OCCUPANCY_RATE,
    GET_ADDRESS,
    GET_ROOMS,
    GET_FREE_ROOMS
 }
