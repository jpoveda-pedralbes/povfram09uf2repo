package cat.inspedralbes.povfra;

class PovFraA5Address {
	private String street; 
	private String city;
	private String province;
	private String country;
	private String zip;	
	
	public PovFraA5Address(String s,String c,String z)
	{
		street=s;
		city=c;
		province=c;
		zip=z;
		country="Spain";
	}
	
	public PovFraA5Address(String s,String c,String p,String z)
	{
		street=s;
		city=c;
		province=p;
		zip=z;
		country="Spain";
	}	
	public PovFraA5Address(String s,String c,String p,String z,String co)
	{
		street=s;
		city=c;
		province=p;
		zip=z;
		country=co;
	}	
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}	
}
