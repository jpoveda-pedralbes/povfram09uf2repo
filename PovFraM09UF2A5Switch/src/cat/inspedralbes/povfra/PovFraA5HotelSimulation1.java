package cat.inspedralbes.povfra;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

public class PovFraA5HotelSimulation1 {
	
	public static void main(String[] args) {
			
		///////////////////////////////////////////////////////////////////////
		// Create a file handler object for logging purposes 
        FileHandler handler=null;
		try {
			handler = new FileHandler("logSimulation1.txt");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        handler.setFormatter(new SimpleFormatter()); 
        ///////////////////////////////////////////////////////////////////////
		
		PovFraA5Hotel hr= new PovFraA5Hotel("IAM",new PovFraA5Address("Avinguda d'Esplugues, 38","Barcelona","08034"),40);						
  
        // Add file handler as  
        PovFraA5Hotel.LOGGER.addHandler(handler);
  
        // Set Logger level() 
        PovFraA5Hotel.LOGGER.setLevel(Level.FINEST);
		
        hr.setFreeRooms(5);
		PovFraA5Client client1 = new PovFraA5Client(hr,PovFraSimulationType.BOOKING,4);		
		PovFraA5Client client2 = new PovFraA5Client(hr,PovFraSimulationType.AVAILABILITY,3);	
		PovFraA5Client client3 = new PovFraA5Client(hr,PovFraSimulationType.BOOKING,2);			
		
		client1.setName("Client D");				
		client2.setName("Client E");			
		client3.setName("Client F");			
		
		client1.start();		
		client2.start();
		client3.start();
		
		
		
	}

}
