package cat.inspedralbes.povfra;

import java.util.logging.Level;
import java.util.logging.Logger;

class PovFraA5Hotel {	
	
	final static Logger LOGGER = Logger.getLogger(PovFraA5Hotel.class.getName());	
	
	private final String name;															
	private final PovFraA5Address address;												
	private final int rooms;															
	private int freeRooms;		
	
	public PovFraA5Hotel(String n, PovFraA5Address a,int r){
		name=n;
		address=a;
		rooms=r;	
		setFreeRooms(rooms);
	}	
	public String getName() { //no sincronitzar
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "getName", name);
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".getName with value "+name+".\n");
		return name;
	}
	public PovFraA5Address getAddress() { //no sincronitzar
//		LOGGER.exiting(PovFraA5Hotel.class.getName(), "getAddress", address);
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".getAddress with value "+address+".\n");
		return address;
	}
	public int getRooms() { //no sincronitzar
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "getRooms", rooms);
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".getRooms with value "+rooms+".\n");
		return rooms;
	}
	
//	public synchronized int getFreeRooms(){ //sincronitzar: variable a la que s'accedeix desde diferents metodes
	public int getFreeRooms(){ //sincronitzar: variable a la que s'accedeix desde diferents metodes
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "getFreeRooms", freeRooms);
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".getFreeRooms with value "+freeRooms+".\n");
		return freeRooms;
	}
	public void setFreeRooms(int fr){ //sincronitzar: variable a la que s'accedeix desde diferents metodes
		LOGGER.finer(Thread.currentThread().getName()+" entering "+PovFraA5Hotel.class.getName()+".setFreeRooms with value "+freeRooms+".\n");
		freeRooms=fr;		
		LOGGER.log(Level.INFO,"Free rooms: "+freeRooms+".\n");
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".setFreeRooms with value "+freeRooms+".\n");				
	}
	
//	public synchronized boolean getAvailability(int numRooms){
	public boolean getAvailability(int numRooms){ //sincronitzar: variable a la que s'accedeix desde diferents metodes
		//LOGGER.entering(PovFraA5Hotel.class.getName(), "getAvailability");
		LOGGER.finer(Thread.currentThread().getName() + " entering " +PovFraA5Hotel.class.getName()+ ".getAvailability.\n");		// Equivalent a .log(Level.FINER,
		LOGGER.info(Thread.currentThread().getName() + " wants "+numRooms+" rooms.\n"); 											// Equivalent a .log(Level.INFO,
		boolean value=false;
		if (numRooms<=freeRooms)
			value=true;		
		LOGGER.info(Thread.currentThread().getName() + " AVAILABILITY "+value+".\n");		
		LOGGER.finer(Thread.currentThread().getName() + " exiting " +PovFraA5Hotel.class.getName()+ ".getAvailability.\n.");	
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "getAvailability", value);
		return value;			
	}
	public double getOccupancyRate(){
//	public synchronized double getOccupancyRate(){ //sincronitzar: s'accedeix a getFreeRooms
		double value=(rooms-getFreeRooms())/rooms;
		LOGGER.finer(Thread.currentThread().getName()+" exiting "+PovFraA5Hotel.class.getName()+".getOccupancyRate with value "+value+".\n");	
		return value;
	}	
//	sincronitzar: variable a la que s'accedeix desde diferents metodes
//	public synchronized void processPetition(int numRooms){ // book: numRooms>0, cancellation: numRooms<0
	public void processPetition(int numRooms){ // book: numRooms>0, cancellation: numRooms<0 / 
		//LOGGER.entering(PovFraA5Hotel.class.getName(), "processPetition");
		LOGGER.finer(Thread.currentThread().getName() + " entering " +PovFraA5Hotel.class.getName()+ ".processPetition.\n");		
		freeRooms = freeRooms - numRooms;		
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "processPetition");
		LOGGER.info(Thread.currentThread().getName() + ": BOOKING "+numRooms+" ROOMS.\n"); 
		LOGGER.info(Thread.currentThread().getName() + ": call ends: "+freeRooms+ " AVAILABLE.\n");
		LOGGER.finer(Thread.currentThread().getName() + " exiting " +PovFraA5Hotel.class.getName()+ ".processPetition.\n");		
	}		
	//Si getAvailability y processPetition fosin private, no s'haurian
	//de sincronitzar perque nomes s'accederien desde aquest classe
//	public synchronized void bookingPetition(int numRoomsToBook){
	public void bookingPetition(int numRoomsToBook){
		//LOGGER.entering(PovFraA5Hotel.class.getName(), "bookingPetition");				
		LOGGER.finer(Thread.currentThread().getName() + " entering " +PovFraA5Hotel.class.getName()+ ".bookingPetition.\n");		
		if(getAvailability(numRoomsToBook))					
			processPetition(numRoomsToBook);		
		//LOGGER.exiting(PovFraA5Hotel.class.getName(), "bookingPetition");
		LOGGER.finer(Thread.currentThread().getName() + " exiting " +PovFraA5Hotel.class.getName()+ ".bookingPetition.\n");
	}
}