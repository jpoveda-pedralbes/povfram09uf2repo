package cat.inspedralbes.povfra;

public class PovFraA7Sala {

	final static int AFORAMENT = 10;
	public static int numPersones;
	
	public synchronized void sortir() {
		numPersones--;
		notify();
	}
	
	public synchronized void entrar() {
		while (numPersones == AFORAMENT) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
		numPersones++;
	}
	
}
