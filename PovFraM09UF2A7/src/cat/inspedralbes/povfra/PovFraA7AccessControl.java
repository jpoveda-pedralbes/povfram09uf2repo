package cat.inspedralbes.povfra;

import java.util.ArrayList;

public class PovFraA7AccessControl {
	
	public static void main(String[] args) {
		ArrayList<PovFraA7Persona> personesArray = new ArrayList<>();
		
		PovFraA7Sala sala = new PovFraA7Sala();

		for(int i=0; i<30; i++) {
			PovFraA7Persona persona = new PovFraA7Persona(sala);
			persona.setName("Persona " + (i+1));
			persona.start();
			personesArray.add(persona);
		}
		
	}
}