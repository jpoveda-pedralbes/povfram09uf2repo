package cat.inspedralbes.povfra;

import java.util.Random;

public class PovFraA7Persona extends Thread implements Runnable{

	private PovFraA7Sala sala;

    public PovFraA7Persona(PovFraA7Sala sala) {
        this.sala = sala;
    }
	
	@Override
	public void run() {
		Random tempsAleatori = new Random();

		try {
            Thread.sleep(tempsAleatori.nextInt(50000));
        } catch (InterruptedException e) {}

		System.out.println("entrar");
		sala.entrar();
		System.out.println(PovFraA7Sala.numPersones+" persones");
		
		try {
            Thread.sleep(tempsAleatori.nextInt(50000));
        } catch (InterruptedException e) {}
		
		System.out.println("sortir");
		sala.sortir();
	}

}
