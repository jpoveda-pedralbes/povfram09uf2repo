package PovFraM09UF2A1;

public class PovFraE2TiradaDaus {
	public static void main( String[] args) {
		
		Thread fil1 = new Thread(new PovFraE2RunnableTirador());
		Thread fil2 = new Thread(new PovFraE2RunnableTirador());
		Thread fil3 = new Thread(new PovFraE2RunnableTirador());
		
		fil1.setName("Fil1");
		fil1.setPriority(Thread.MAX_PRIORITY);
		fil2.setName("Fil2");
		fil2.setPriority(Thread.NORM_PRIORITY);
		fil3.setName("Fil3");
		fil3.setPriority(Thread.MIN_PRIORITY);
		
		fil1.start();
		fil2.start();
		fil3.start();
		
		try {
			fil1.join();
			fil2.join();
			fil3.join();
		}catch( Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Tirada finalitzada.");
	}
}
