package PovFraM09UF2A1;

import java.util.ArrayList;

/*
 * RESPOSTES ACTIVITAT
 * 
 * Exercici 1
 * a. S'executa cada thread amb 5 segons de diferencia entre ells i el nom del fil al imprimir no es l'indicat.
 * b. No s'executen en l'ordre indicat i a mes la frase del final s'executa primer.
 * c. 
 */

public class PovFraE1TiradaDaus {
	public static void main( String[] args) {

		if (args.length !=1)
		{
			System.exit(1);
		} else {
			int numJugadors = Integer.parseInt(args[0]);

			ArrayList<Thread> fils = new ArrayList<Thread>();

			if( numJugadors > 0) {
				for(int i=1; i<=numJugadors; i++) {
					Thread fil = new Thread( new PovFraE1TiradorSenzill());
					fil.setName("fil " + (i));
					if( i == 2 ) {
						fil.setPriority(Thread.MAX_PRIORITY);
					}else {
						fil.setPriority(Thread.MIN_PRIORITY);
					}
					fils.add(fil);
				}
			}
			
			if( !fils.isEmpty() ) {
				for(int i=0; i<fils.size(); i++) {
					fils.get(i).start();
					try {
						fils.get(i).join();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			System.out.println("Tirada finalitzada.");
		}
		
	}
}
